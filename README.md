## Getting Started

1. Open a terminal, command prompt, or PowerShell and change to your working directory (e.g. U:)
2. Grab a copy of lab09 using: `git clone https://bitbucket.org/csis10a/lab09.git`
3. Follow the instructions at [https://docs.google.com/document/d/1X_DCAAJBJNpasg8On4UFw0NbH1A-ZsnG9gG2zXbBRno/edit?usp=sharing](https://docs.google.com/document/d/1X_DCAAJBJNpasg8On4UFw0NbH1A-ZsnG9gG2zXbBRno/edit?usp=sharing)

__Note:__ If git is not installed on your computer you can download the lab from [https://bitbucket.org/csis10a/lab09/get/master.zip](https://bitbucket.org/csis10a/lab09/get/master.zip)
